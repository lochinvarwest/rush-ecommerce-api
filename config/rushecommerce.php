<?php

return [
    'account' => [
        'token' => env('RUSH_AUTHTOKEN', null),
        'account_email' => env('RUSH_ACCOUNT_EMAIL', null),
    ]
];

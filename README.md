# Laravel integration with Rush E-Commerce API.
* This package handles the transport layer between your application and the Rush E-Commerce API ( https://www.rush.co.za ).  
* It does not help create / format the body / packets sent to Rush.  Please refer to the Rush E-Commerce API documentation for the required fields and formats.
* This package does however encode the packets passed to it into JSON, so please pass your arguments as arrays where applicable.
* You will require a Rush E-Commerce API account to get the API Auth Token.
    
## Installation
* Run the following in your project root 
```
composer require "lochinvarwest/rush-ecommerce-api:^0.1"
```

#### Publish the config file

```
php artisan vendor:publish --provider="LochinvarWest\Rush\RushServiceProvider" --tag="config"
```    

#### Update the config settings.

* Update the config settings in config/rushecommerce.php or insert the relevant .env variables.

```php
return [
    'account' => [
        'token' => env('RUSH_AUTHTOKEN', null),
        'account_email' => env('RUSH_ACCOUNT_EMAIL', null),
    ]
];
```

## Usage

* Standalone using the Handler.

```php
$handler = RushHandler::create();
// or to override the default account in the config
$handler = RushHandler::create(['token' => $token, 'account_email' => $account_email]);
// then
$handler->getQuotes($deliveryDetails); //for instance
```

* Using method injection with container registration from the service provider.

```php
class RushController
{
    public function index(RushHandler $handler)
    {
        $handler->getQuotes($deliveryDetails);
    }   
}
```

## Available API calls:

#### getQuotes($deliveryDetails)
$deliveryDetails is an array (not json) as per the Rush API documentation (including all volumetric data, pick up, drop off details).

The function returns an object (RushObject) of the reply or throws a RushException with the relevant API or transport error message.
```php
$handler->getQuotes($deliveryDetails);
```
Or to select the cheapest / fastest quote:
```php
$handler->getQuotes($deliveryDetails)->cheapest();
$handler->getQuotes($deliveryDetails)->fastest();
```

#### confirmBooking($bookingDetail)
$bookingDetails is an array of as per the Rush API documentation (all volumetric data, supplier and delivery address along with selected carrier service).

The function returns a 'success' object with the waybill number or throws a RushException with the relevant API or transport error message.
```php
$handler->confirmBooking($bookingDetail);
```

#### getPdfWaybill($waybillNumbers)
$waybillNumbers is an array of waybill numbers.
```php
$handler->getPdfWaybill(['waybills' => [] ]);
```

The function returns an array of objects of URL's to the PDF waybills or throws a RushException with the relevant API or transport error message.

#### trackWaybill($waybillNumber)
$waybillNumber is the number received from a confirmBooking call.

The function returns an array of the tracking information or throws a RushException with the relevant API or transport error message.
```php
$handler->trackWaybill($waybillNumber);
```

#### addressListings()
This function simply returns the JSON list of valid SUBURB-TOWN-POSTCODE address combinations for getting quotes or making bookings on the Rush API.  (there are ~20 000 of them)
```php
$handler->addressListings();
```

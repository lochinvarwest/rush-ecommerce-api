<?php

namespace LochinvarWest\Rush;

use LochinvarWest\Rush\Exceptions\RushException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use LochinvarWest\Rush\Objects\RushObject;

class RushHandler
{
    protected $token;
    protected $email;
    protected $url;
    protected $headers;
    protected $quotes;

    const RUSH_TEST_URL = "https://rush.test.jini.guru/api/v2/";
    const RUSH_BASE_URL = "https://www.rush.co.za/api/v2/"; // Live API

    public function __construct($account = null)
    {
        if (!isset($account)) {
            $account = config('rushecommerce.account');
        }

        if (!$this->token = $account['token']) {
            throw new RushException('The Rush Ecommerce authorisation token is not set', 404);
        }

        if (!$this->email = $account['account_email']) {
            throw new RushException('The Rush Ecommerce account email is not set', 404);
        }

        $this->url = env('APP_ENV') === 'production' ? self::RUSH_BASE_URL : self::RUSH_TEST_URL;

        $this->headers = [
            'X-Auth-Token' => $this->token,
            'Rush-email' => $this->email
        ];

    }

    public static function create($account = null)
    {
        return new self($account);
    }

    public function getQuotes($deliveryDetail)
    {
        try {
            $client = new Client();
            $result = $client->request('POST', $this->url.'costComparison', [
                'headers' => $this->headers,
                'json' => $deliveryDetail
            ])->getBody()->getContents();

            return new RushObject($this->returnResult($result));

        } catch (ServerException|ClientException|RequestException $e) {
            throw new RushException('Rush error: ' . $e->getMessage(), 404);
        }

    }

    public function confirmBooking($bookingDetail)
    {
        try {
            $client = new Client();
            $result = $client->request('POST', $this->url.'bookingConfirmation', [
                'headers' => $this->headers,
                'json' => $bookingDetail
            ])->getBody()->getContents();

            return $this->returnResult($result);

        } catch (ServerException|ClientException|RequestException $e) {
            throw new RushException('Rush error: ' . $e->getMessage(), 404);
        }

    }

    public function getPdfWaybill($waybillNumbers)
    {
        try {
            $client = new Client();
            $result = $client->request('POST', $this->url.'generatePDF', [
                'headers' => $this->headers,
                'json' => $waybillNumbers
            ])->getBody()->getContents();

            return $this->returnResult($result);

        } catch (ServerException|ClientException|RequestException $e) {
            throw new RushException('Rush error: ' . $e->getMessage(), 404);
        }

    }

    public function trackWaybill($waybillNumber)
    {
        try {
            $client = new Client();
            $result = $client->request('GET', $this->url.'trackWaybill/'.$waybillNumber, [
                'headers' => $this->headers
            ])->getBody()->getContents();

            return $this->returnResult($result);

        } catch (ServerException|ClientException|RequestException $e) {
            throw new RushException('Rush error: ' . $e->getMessage(), 404);
        }

    }

    public function addressListings()
    {
        try {
            $client = new Client();
            $result = $client->request('GET', 'https://www.rush.co.za/ecommerce/address/addressListingEcommerceApi.json')->getBody()->getContents();

            return $this->returnResult($result);

        } catch (ServerException|ClientException|RequestException $e) {
            throw new RushException('Rush error: ' . $e->getMessage(), 404);
        }

    }

    private function returnResult($result)
    {
        $result = json_decode($result);

        if (isset($result->error)) {
            throw new RushException('Rush error: ' . $result->error->message, $result->error->status_code);
        }

        return $result;
    }

}

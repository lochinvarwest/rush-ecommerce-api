<?php


namespace LochinvarWest\Rush\Objects;


use LochinvarWest\Rush\Actions\GetFastestCostQuote;
use LochinvarWest\Rush\Actions\GetLowestCostQuote;

class RushObject
{
    public function __construct($object)
    {
        $this->object = $object;
    }

    public function cheapest()
    {
        return GetLowestCostQuote::run($this);
    }

    public function fastest()
    {
        return GetFastestCostQuote::run($this);
    }

}

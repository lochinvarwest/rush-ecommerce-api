<?php


namespace LochinvarWest\Rush\Actions;


class GetLowestCostQuote
{

    public static function run($quotes)
    {
        $collection = collect($quotes->object->data->CostComparisonResult->CostComparisonResults->ResultSet->Result);
        $lowestCost = $quotes->object->data->CostComparisonResult->CostComparisonResults->Statistics->LeastCostIndex;

        return $collection->filter(function($value) use($lowestCost) {
            return $value->RecordId == $lowestCost;
        });
    }

}

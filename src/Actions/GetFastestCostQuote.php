<?php


namespace LochinvarWest\Rush\Actions;


class GetFastestCostQuote
{

    public static function run($quotes)
    {
        $collection = collect($quotes->object->data->CostComparisonResult->CostComparisonResults->ResultSet->Result);
        $fastestCost = $quotes->object->data->CostComparisonResult->CostComparisonResults->Statistics->FastestCostIndex;

        return $collection->filter(function($value) use($fastestCost) {
            return $value->RecordId == $fastestCost;
        });
    }

}

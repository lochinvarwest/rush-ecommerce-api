<?php

namespace LochinvarWest\Rush;

use Illuminate\Support\ServiceProvider;

class RushServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/rushecommerce.php' => base_path('config/rushecommerce.php'),
        ], 'config');

    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/rushecommerce.php', 'rushecommerce');

        $this->app->bind(RushHandler::class, function ($app) {
            $config = $app->make('config')->get('rushecommerce');

            return RushHandler::create($config['account']);
        });

    }
}
